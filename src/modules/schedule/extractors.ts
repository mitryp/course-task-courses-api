import {CourseSeason, EducationLevel, HtmlKeyExtractor, InterfaceHtmlExtractor, Nullable} from '../../common/types';
import {IScheduleItem} from './types';
import * as cheerio from 'cheerio';
import {CheerioAPI, Element} from 'cheerio';
import {emptyString, none, trim} from "../../common/normalization";
import {seasonNamesToSeasons} from "../../common/mappings";
import {datetimePattern, levelYearPattern, seasonNamePattern} from "./regexps";


export const scheduleFieldExtractors: InterfaceHtmlExtractor<Element, Nullable<IScheduleItem>> = {
    updatedAt: [withPanelText(parseDatetime), none],
    url: [parseUrl, trim],
    season: [emptyString, none],
    facultyName: [emptyString, none],
}

function extractLevelYear(filename: string): {
    level: EducationLevel,
    year: number,
} | undefined {
    const levelYear = filename.match(levelYearPattern)?.at(0)?.split('-');

    if (!levelYear)
        return undefined;

    return {
        level: levelYear[0] === 'БП' ? EducationLevel.BACHELOR : EducationLevel.MASTER,
        year: +levelYear[1]
    };
}

function extractSeason(filename: string) {
    const seasonName = filename.match(seasonNamePattern)!.at(0)!;
    return seasonNamesToSeasons[seasonName];
}

export function extractDataFromFilename(e: Element, $: CheerioAPI): {
    specialityName: string,
    level: EducationLevel,
    year: number,
    season: CourseSeason
} {
    const filename = $('a[title="Завантажити"]', e).text()
        .replace('Завантажити', '').trim();

    const levelYear = extractLevelYear(filename);
    const specialityName = filename.split(levelYearPattern)[0];
    const season = extractSeason(filename);

    return {
        specialityName: trim(specialityName),
        level: levelYear!.level,
        year: levelYear!.year,
        season: season,
    };
}

function parseUrl(e: cheerio.Cheerio<cheerio.Element>, n$?: CheerioAPI): string {
    const $ = n$!;
    const url = $('a[title="Завантажити"]', e).attr('href');

    return url ?? '';
}

function parseDatetime(panelText: string): string {
    const datetime = panelText.match(datetimePattern)?.at(0);
    if (!datetime) return '';

    const [[day, month, year], [hour, minute, second]] =
        datetime.split(' ')
            .map((str, i) => str.split(i === 0 ? '.' : ':'));

    return `${[year, month, day].join('-')} ${[hour, minute, second].join(':')}`;
}

function withPanelText(textExtractor: (text: string) => string): HtmlKeyExtractor<cheerio.Element, IScheduleItem> {
    return (e, $) => textExtractor($!(e).text());
}

export function parseFaculty(e: cheerio.Element, $: CheerioAPI): string {
    return $('div.panel-heading[id^=schedule-faculty]', e).text();
}