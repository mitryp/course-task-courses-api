import {Module} from '@nestjs/common';
import {ScheduleController} from './schedule.controller';
import {ScheduleService} from './schedule.service';
import {ScheduleParseService} from "./schedule.parse.service";
import {ScheduleFetchService} from "./schedule.fetch.service";

@Module({
    imports: [],
    controllers: [ScheduleController],
    providers: [ScheduleService, ScheduleParseService, ScheduleFetchService],
})
export class ScheduleModule {
}
