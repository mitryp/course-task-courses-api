import {Controller, Get, NotFoundException, Param} from '@nestjs/common';
import {ScheduleService} from './schedule.service';
import {resultToResponse} from "../../common/result";
import {HttpErrorByCode} from "@nestjs/common/utils/http-error-by-code.util";

@Controller('schedule')
export class ScheduleController {
    constructor(protected readonly service: ScheduleService) {
    }

    @Get([':year/:season'])
    public async getSchedule(@Param('year') year: number, @Param('season') season: string) {
        const res = await this.service.getSchedule(year, season);

        if (res.hasResult && res.result.length === 0)
            throw new NotFoundException();

        return resultToResponse(res);
    }

}
