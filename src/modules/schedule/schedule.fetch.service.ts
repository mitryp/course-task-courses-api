import {Injectable} from "@nestjs/common";
import {fetchDataAndWrap} from "../../common/fetchDataAndWrap";
import {Result} from "../../common/result";

const yearPlaceholder = '$YEAR$';
const seasonPlaceholder = '$SEASON$';
const scheduleUrl = `https://my.ukma.edu.ua/schedule?year=${yearPlaceholder}&season=${seasonPlaceholder}`;

@Injectable()
export class ScheduleFetchService {
    public async fetchSchedule(year: number, seasonOrdinal: number): Promise<Result<any>> {
        const url = scheduleUrl.replace(yearPlaceholder, year.toString())
            .replace(seasonPlaceholder, seasonOrdinal.toString());

        return fetchDataAndWrap(url);
    }
}
