import {Injectable} from '@nestjs/common';
import {ScheduleFetchService} from "./schedule.fetch.service";
import {ScheduleParseService} from "./schedule.parse.service";
import {Result} from "../../common/result";
import {seasonNamesEng} from "../../common/mappings";

@Injectable()
export class ScheduleService {

    constructor(private scheduleFetcher: ScheduleFetchService, private scheduleParser: ScheduleParseService) {
    }

    public async getSchedule(year: number, season: string) {
        const seasonOrdinal = seasonNamesEng.findIndex(s => s === season.toLowerCase());

        const fetched = await this.scheduleFetcher.fetchSchedule(year, seasonOrdinal);
        if (!fetched.hasResult)
            return fetched;

        return Result.ok(this.scheduleParser.parseSchedule(fetched.result));
    }
}
