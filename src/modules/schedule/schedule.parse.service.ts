import {Injectable} from '@nestjs/common';
import {IScheduleItem} from './types';
import * as cheerio from 'cheerio';
import {CheerioAPI, Element} from 'cheerio';
import {extractDataFromFilename, parseFaculty, scheduleFieldExtractors} from "./extractors";
import {InterfaceHtmlExtractor} from "../../common/types";
import {trim} from "../../common/normalization";

@Injectable()
export class ScheduleParseService {
    public parseSchedule(schedulePage: string): IScheduleItem[] {
        const $ = cheerio.load(schedulePage);

        return $('div.panel.panel-info').toArray()
            .flatMap(facultyPanel => parseFacultyPanelItems(facultyPanel, $));
    }
}

function parseFacultyPanelItems(facultyPanel: Element, $: CheerioAPI): IScheduleItem[] {
    const facultyName = trim(parseFaculty(facultyPanel, $));

    return $('li.list-group-item', facultyPanel)
        .toArray()
        .map(scheduleItemPanel => parseScheduleItemPanel(scheduleItemPanel, $))
        .map(i => {
            i.facultyName = facultyName;
            return i;
        });
}

function parseScheduleItemPanel(panel: cheerio.Element, $: CheerioAPI): IScheduleItem {
    const obj: any = {};

    const fieldExtractors = scheduleFieldExtractors as InterfaceHtmlExtractor<Element, IScheduleItem>;
    for (const key in scheduleFieldExtractors) {
        const scheduleKey = key as keyof IScheduleItem;
        const [
            extractor,
            normalizer
        ] = fieldExtractors[scheduleKey];

        obj[key] = normalizer(extractor(panel, $));
    }

    return {
        ...obj,
        ...extractDataFromFilename(panel, $),
    }
}



