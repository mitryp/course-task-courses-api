// БП-1, МП-2 etc.
export const levelYearPattern = /((БП)|(МП))-(\d)/gm;

// DD.MM.YYYY HH:mm:ss
export const datetimePattern = /\d\d\.\d\d\.\d{4} \d\d:\d\d:\d\d/gm;

export const seasonNamePattern = /(Осінь|Весна|Літо)/gm;
