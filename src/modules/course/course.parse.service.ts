import {Injectable} from '@nestjs/common';
import * as cheerio from 'cheerio';
import {courseFieldExtractors} from './extractors';
import {ICourse} from './types';

@Injectable()
export class CourseParseService {
    public parseCourse(coursePage: string): ICourse {
        const $ = cheerio.load(coursePage);
        const obj: any = {};

        for (const key in courseFieldExtractors) {
            const courseKey = key as keyof ICourse;
            const [
                extractor,
                normalizer
            ] = courseFieldExtractors[courseKey];

            const value = normalizer(extractor($));
            obj[courseKey] = (value || typeof value === 'number') ? value : undefined;
        }

        return obj;
    }
}

