import {BadRequestException, Controller, Get, Param} from '@nestjs/common';
import {CourseService} from './course.service';
import {resultToResponse} from '../../common/result';
import {ICourse} from "./types";

@Controller('course')
export class CourseController {
    constructor(
        protected readonly service: CourseService,
    ) {
    }

    @Get(':code')
    public async getCourse(@Param('code') code: number): Promise<ICourse> {
        // @ts-ignore
        if (!(code as string).match(/\d+/gm)) // та, хто б міг подумати
            throw new BadRequestException();

        return resultToResponse(await this.service.getCourse(+code));
    }
}
