import {Injectable} from '@nestjs/common';
import {Result} from '../../common/result';
import {fetchDataAndWrap} from '../../common/fetchDataAndWrap';

const codePlaceholder = '$CODE$';
const coursesResource = `https://my.ukma.edu.ua/course/${codePlaceholder}`;

@Injectable()
export class CourseFetchService {
    public async fetchCourse(code: number): Promise<Result<any>> {
        const courseUrl = coursesResource.replace(codePlaceholder, code.toString());

        return fetchDataAndWrap(courseUrl);
    }
}
