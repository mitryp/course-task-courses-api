import {Injectable} from '@nestjs/common';
import {ICourse} from './types';
import {Result} from '../../common/result';
import {CourseFetchService} from './course.fetch.service';
import {CourseParseService} from './course.parse.service';

@Injectable()
export class CourseService {
    constructor(private readonly courseFetcher: CourseFetchService, private readonly courseParser: CourseParseService) {
    }

    public async getCourse(code: number): Promise<Result<ICourse>> {
        const fetched = await this.courseFetcher.fetchCourse(code);

        if (!fetched.hasResult)
            return fetched;

        const course = this.courseParser.parseCourse(fetched.result);
        return Result.ok(course);
    }
}
