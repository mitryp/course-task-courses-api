import {Module} from '@nestjs/common';
import {CourseController} from './course.controller';
import {CourseService} from './course.service';
import {CourseFetchService} from './course.fetch.service';
import {CourseParseService} from './course.parse.service';

@Module({
    imports: [],
    controllers: [CourseController],
    providers: [CourseService, CourseFetchService, CourseParseService],
})
export class CourseModule {
}
