import {CheerioAPI} from "cheerio";
import {CourseSeason, EducationLevel, InterfaceHtmlExtractor} from "../../common/types";
import {ICourse} from "./types";
import {firstElementToNum, none, toNum, trim} from "../../common/normalization";
import {levelNamesToLevels, seasonNamesToSeasons} from "../../common/mappings";

function selectSeasons($: CheerioAPI): CourseSeason[] {
    const seasonNamesText =
        $('th:where(:contains("Осінь"), :contains("Весна"), :contains("Літо"))').text();

    return seasonNamesText.trim().split(/\s+/gm).map(n => seasonNamesToSeasons[n]);
}

function selectEducationLevel($: CheerioAPI): EducationLevel {
    const levelText = $('th:contains("Освітній рівень")+td').text().trim();
    return levelNamesToLevels[levelText];
}

function selectCourseCode($: CheerioAPI) {
    return $('th[title="Код курсу"]+td').text();
}

function selectTitle($: CheerioAPI) {
    return $('title').text();
}

function selectDescription($: CheerioAPI) {
    return $('div[id$="--info"]').text();
}

function selectFacultyName($: CheerioAPI) {
    return $('th:has(abbr[title^="Факультет"])+td').text();
}

function selectDepartmentName($: CheerioAPI) {
    return $('th:has(abbr[title^="Кафедра"])+td').text();
}

function selectYear($: CheerioAPI) {
    return $('span[title$="рік викладання"]').text();
}

function selectCreditsAmount($: CheerioAPI) {
    return $('span[title$="кредитів"]').text();
}

function selectHoursAmount($: CheerioAPI) {
    return $('span[title$="годин"]').text();
}

function selectTeacherName($: CheerioAPI) {
    return $('th:contains("Викладач")+td').text();
}

export const courseFieldExtractors: InterfaceHtmlExtractor<CheerioAPI, ICourse> = {
    code: [selectCourseCode, toNum],
    name: [selectTitle, trim],
    description: [selectDescription, trim],
    facultyName: [selectFacultyName, trim],
    departmentName: [selectDepartmentName, trim],
    level: [selectEducationLevel, none],
    year: [selectYear, firstElementToNum],
    seasons: [selectSeasons, none],
    creditsAmount: [selectCreditsAmount, firstElementToNum],
    hoursAmount: [selectHoursAmount, firstElementToNum],
    teacherName: [selectTeacherName, trim],
}
