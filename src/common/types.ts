import {CheerioAPI} from 'cheerio';

export enum EducationLevel {
    BACHELOR = 'Bachelor',
    MASTER = 'Master',
}

export enum CourseSeason {
    AUTUMN = 'Autumn',
    SPRING = 'Spring',
    SUMMER = 'Summer',
}

export type Nullable<T> = T | null | undefined;

export type HtmlKeyExtractor<E, T> = (e: E, $?: CheerioAPI) => string | string[];
export type ValueNormalizer<T> = (val: any) => T[keyof T];
export type InterfaceHtmlExtractor<E, T> = Record<keyof T, [HtmlKeyExtractor<E, T>, ValueNormalizer<T>]>;