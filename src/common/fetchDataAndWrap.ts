import {Result} from './result';
import axios, {AxiosError} from 'axios';

export function fetchDataAndWrap<T>(url: string): Promise<Result<T>> {
    return axios.get(url)
        .then(value => Result.ok(value.data))
        .catch((reason: AxiosError): Result<T> => {
            if (reason.response?.status === 404)
                return Result.notFound();

            return Result.error();
        });
}