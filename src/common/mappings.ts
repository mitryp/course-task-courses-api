import {CourseSeason, EducationLevel} from './types';

export const levelNamesToLevels: Record<string, EducationLevel> = {
    'Бакалавр': EducationLevel.BACHELOR,
    'Магістр': EducationLevel.MASTER,
} as const;

export const seasonNamesToSeasons: Record<string, CourseSeason> = {
    'Осінь': CourseSeason.AUTUMN,
    'Весна': CourseSeason.SPRING,
    'Літо': CourseSeason.SUMMER,
} as const;

export const seasonNamesEng: string[] = [CourseSeason.AUTUMN, CourseSeason.SPRING, CourseSeason.SUMMER]
    .map(s => s.toLowerCase());