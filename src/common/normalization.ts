export function trim<T>(str: any): T[keyof T] {
    return str?.trim();
}

export function firstElementToNum<T>(value: any): T[keyof T] {
    const firstElement = value?.trim().split(' ')[0];
    return toNum(firstElement);
}

export function toNum<T>(value: any): T[keyof T] {
    return value ? +value : undefined as any;
}

export function none<T>(v: any): T[keyof T] {
    return v;
}

export function emptyString() {
    return '';
}